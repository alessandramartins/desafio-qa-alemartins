#Encoding: UTF-8
Feature: PBI_01.01 User View Profile Image and Change in WebWhatsapp
	To check the functionality of viewing and changing the user profile photo
    As webwhatsapp user
    I want to see and change my profile photo
	
Background: 
	Given that I have previously read the QR Code to be able to access the webwhatsapp by the Firefox browser
    And access the webwhatsapp by url 
    And I'm on the WebWhatsapp home screen
	
@Cenario1	
Scenario: Click on Profile Image and View
	Given I am logged in the webwhatsapp
	When I Click on Avatar Image icon
	Then I Should be redirected to the My Profile settings
	When I Click the Avatar Image text
	Then I will be redirect to  view my profile image larger and centralized
	When I Click on X button 
	Then I Should be redirected to my profile configuration menu
	
	
@Cenario2	
Scenario: Click on Profile Image and Remove
	Given I am logged in the webwhatsapp
	When I Click on Avatar Image icon
	Then I Should be redirected to the My Profile settings
	When I Click the Avatar Image option
	And I Click to choose text option remove photo
	Then popup asking for confirmation to remove the photo
    And I click the remove button
    And the Removed Profile Photo Message should be displayed
	
package webwhatsapptest;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Stepwebwhatsapp extends Step {
	@Before
	  public void setUp() throws Exception {
	    private final WebDriver driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Given("^ that I have previously read the QR Code to be able to access the webwhatsapp by the Firefox browser$")
	public void able_to_access_webwhatsapp_by_url(){
	        driver.get("https://web.whatsapp.com/")
}
    @Given("^access the webwhatsapp by url$")
    public void acess_the_webwhatsapp_by_url throws Throwable {
		assertEquals("Mantenha seu telefone conectado", driver.findElement(By.id("//*[@id='app']/div/div/div[3]/div/div/div[2]/h1")).getText());
	}

	@Given("^I'm on the WebWhatsapp home screen$")
	public void I_m_on_the_webwhatspp_home_screen() throws Throwable {
		assertTrue("Mantenha seu telefone conectado", driver.findElement(By.id("//*[@id='side']/header/div[1]")).getImage());
	}

	@Given("^I am logged in the webwhatsapp$")
	public void I_m_logged_in_the_webwhatsapp() throws Throwable {
		 assertTrue("//*[@id='app']/div/div/div[3]/div/div/div[2]/div[1]", driver.getText());
	}

	@When("^I Click on Avatar Image icon$")
	public void I_Click_on_Avatar_Image_icon() throws Throwable {
		driver.findElement(By.cssSelector("img.avatar-image.is-loaded")).click();
	}
	
	@When("^I Click the Avatar Image text$")
	public void I_Click_the_Avatar_Image_text() throws Throwable {
		driver.findElement(By.linkText("Ver foto")).click();
	}

	@When("^I Click on X button$")
	public void I_Click_on_X_button() throws Throwable {
		driver.findElement(By.cssSelector("button.icon.icon-x-viewer")).click();
	}

	@When("^I Click the Avatar Image option$")
	public void I_Click_the_Avatar_Image_option() throws Throwable {
		driver.findElement(By.cssSelector("img.avatar-image.is-loaded")).click();
	}
	
	@When("^I Click to choose text option remove photo$")
	public void I_Click_to_choose_text_option_remove_photo() throws Throwable {
		 driver.findElement(By.linkText("Remover foto")).click();
	}

	@Then("^I Should be redirected to the My Profile settings$")
	public void I_Should_be_redirected_to_the_My_Profile_settings() throws Throwable {
		 driver.findElement(By.cssSelector("div.avatar-overlay")).click();
		 assertEquals("Seu Nome", driver.findElement(By.id("//*[@id='app']/div/div/div[1]/span[1]/div/div/div/div[2]/div[1]/div/div")).getText());
	}

	@Then("^I will be redirect to  view my profile image larger and centralized$")
	public void I_will_be_redirect_to_view_my_profile_image_larger_and_centralized() throws Throwable {
		 assertTrue("emojitext", driver.findElement(By.id("//*[@id='app']/div/span[2]/div/div/div[1]/div[1]/div[2]/div/span")).getText());
	}

	@Then("I Should be redirected to my profile configuration menu$")
	public void Report_the_following_information_for_the_new_device(DataTable table) throws Throwable {
		 assertTrue("Perfil", driver.findElement(By.id("//*[@id='app']/div/div/div[1]/span[1]/div/div/header/div/span")).getText());
	}
    
	@Then("popup asking for confirmation to remove the photo$")
	public void popup_asking_for_confirmation_to_remove_the_photo() throws Throwable {
		 assertTrue("Apagar sua foto de perfil", driver.findElement(By.id("//*[@id='app']/div/span[3]/div/div/div/div/div/div[1]/div")).getText());
	}	
	
	@Then("I click the remove button$")
	public void I_click_the_remove_button() throws Throwable {
		 driver.findElement(By.id("//div[@id='app']/div/span[3]/div/div/div/div/div/div[2]/button[2]")).click();
	}

	@Then("the Removed Profile Photo Message should be displayed$")
	public void the_Removed_Profile_Photo_Message_should_be_displayed() throws Throwable {
		assertTrue("Foto de perfil removida",driver.findElement(By.id("//*[@id='app']/div/span[1]/div/span")).getText());
	}
	
	
	@After
	public void close_driver () throws Exception {
		driver.quit();
	}
}

#Encoding: UTF-8
Feature: PBI_02.01 User creates a group, leaves the group, and deletes the group
	To check the functionality of creation, exit, and delete groups
    As the Android whatsapp user
    I want to create a group, leave the group and delete the group that you create
	
Background: 
	Given that I already have Whatsapp installed and pre-configured on my android device
    And access the application by clicking on the whatsapp icon
    And I'm on the Android Whatsapp home screen
	
@Cenario1	
Scenario: Create a Group in Whatsapp
	Given I am logged in the whatsapp for Android
	When I Click on more options icon
	Then I choose New Group option
	When I Click to select the contacts that will be part of the group
	Then I click the next button
	When I type a name for the group and click the next button
	Then I am redirected to the group that has the name that I typed and the participants that I selected
	
	
@Cenario2	
Scenario: Exit Group and Delete Whatsapp Group
	Given I'm in the group chat
    When I click on more options icon in group info
    Then I'm directed to the group confinging screen
    When I click on the icon leave the group
    Then A confirmation popup is displayed to confirm the action
    And I click the exit link
    When I leave the group, I return to the group setup screen
    Then I click the delete group link
    And an action confirmation popup is displayed for me to confirm my choice
    When  I click the button to confirm the delete action
    Then I'm redirected to the home screen of my whatspp and the group was removed from the screen
	
import java.io.File;
import com.robotium.solo.Solo;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import junit.framework.TestCase;


    
public class WhatsappStepsTest extends TestCase {
	 Solo solo;
	 
	 public WhatsappStepsTest (){
		 super ();
	 }
	@RunWith(AndroidJUnit4.class)
    public class WhatsappStepsTest {

    private static final String name = "Name";
    private static final String group_name = "Group Name";


    @Rule
    public ActivityTestRule<Whatsapp> activityTestRule =
            new ActivityTestRule<>(Whatsapp.class);

    private Solo solo; 
	 
	 @Before
	 public void setUp() throws Exception {
		 solo = new Solo(InstrumentationRegistry.getInstrumentation(),activityTestRule.getActivity());
     }
	 
	 @After
	  public void tearDown() throws Exception {
		  solo.finishOpenedActivities();
	  } 
	 
	 @Test
	 public void WhatsappStepsTest(){
		        
		    solo.unlockScreen();
		    solo.clickOnView(solo.getText("Whatsapp")); 
		//@Scenario 1		
		//I am logged in the whatsapp for Android//
			solo.assertCurrentActivity("Expected Whatsapp Activity", Whatsapp.class);
		//I Click on more options icon//
			solo.clickOnButton(solo.getView("More Options");
		//I choose New Group option//
		 solo.clickOnMenuItem(solo.getText("New Group"));
		//I Click to select the contacts that will be part of the group//
			 solo.clearEditText(0);
			 solo.enterText(0, Matheus);
			 solo.typeText(EditText)solo.getView(com.whatsapp:id/search_src_text), "Matheus");
			 solo.clickOnText("Matheus");
	    //I click the next button
	         solo.clickOnButton(com.whatsapp:id/next_btn));
	    //I type a name for the group and click the next button//
	    	 solo.clearEditText(0);
			 solo.enterText(0,"Grupo Teste");
			 solo.typeText(EditText)solo.getView(com.whatsapp:id/group_name), "Grupo Teste");
			 solo.clickOnButton(com.whatsapp:id/ok_btn);
		//I am redirected to the group that has the name that I typed and the participants that I selected//
	    	assertTrue(solo.getTextView(com.whatsapp:id/conversation_contact_name));
	   
	   //@Scenario2	
	    //I'm in the group chat
			solo.assertCurrentActivity("Expected Whatsapp Activity", Whatsapp.class);
			assertTrue(solo.getView(com.whatsapp:id/conversation_contact_name));
		//I click on more options icon in group
			solo.clickOnButton(solo.getView("More Options");
			solo.clickOnMenuItem("Group info");
		//I'm directed to the group confinging screen//
			assertTrue(solo.getTextView(com.whatsapp:id/conversation_contact_status));
		//I click on the icon leave the group
			solo.clickOnButton(solo.getImageView(com.whatsapp:id/exit_group_icon));
		//A confirmation popup is displayed to confirm the action
	    	assertTrue(solo.getTextView(com.whatsapp:id/message)); 
	    //^I click the exit link//
	        solo.clickOnButton("Exit");
		//I leave the group, I return to the group setup screen//
	    	assertTrue(solo.getTextView(com.whatsapp:id/conversation_contact_name));
	    //^I click the delete group link//
	    	solo.clickOnButton(com.whatsapp:id/exit_group_text);
		//an action confirmation popup is displayed for me to confirm my choice//
	    	assertTrue(solo.getTextView(android:id/message)); 
	   	//I click the button to confirm the delete action//
	    	solo.clickOnButton(android:id/button1);
	    //^I'm redirected to the home screen of my whatspp and the group was removed from the screen//
	    	assertTrue(solo.getTextView(com.whatsapp:id/tab));
	    }

}


}
